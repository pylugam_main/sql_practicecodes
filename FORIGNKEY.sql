SELECT * FROM sales_details ;
SELECT * FROM movies ;
UPDATE sales_details SET name = 'kannan' WHERE id = 5 ;
DROP TABLE sales_details ;

CREATE TABLE sales_details(
sales_bill SERIAL PRIMARY KEY,
id INT REFERENCES movies(id),
sales_qty INT
);
INSERT INTO sales_details(id ,sales_qty) VALUES (1,100),(3,200),(4,300),(5,400),(6,500);
UPDATE movies SET  name ='kannan' WHERE id = 3 ; 
UPDATE movies SET collection = 100 WHERE id = 1 ;
DROP TABLE movies ;
