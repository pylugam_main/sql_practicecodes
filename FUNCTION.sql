 CREATE OR REPLACE FUNCTION get_salery(j_id integer)
RETURNS integer AS
$$
DECLARE
sal integer ;
BEGIN
 SELECT collection INTO sal FROM movies WHERE id = j_id ;
 RETURN sal ;
END ;
$$
LANGUAGE plpgsql
DROP FUNCTION  get_salery(j_id integer);

SELECT * FROM get_salery(3);