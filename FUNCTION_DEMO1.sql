 CREATE OR REPLACE FUNCTION get_col(k_id integer)
RETURNS integer AS
$$
DECLARE
 amount integer ;
 BEGIN
 SELECT collection INTO amount FROM movies WHERE id = k_id ;
 RETURN amount ;
 END ;
 $$
 LANGUAGE plpgsql
 DROP FUNCTION get_col(id integer)
 SELECT * FROM get_col(5);
 SELECT * FROM movies;
 
 
 