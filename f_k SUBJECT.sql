 CREATE TABLE subject(
s_id SERIAL PRIMARY KEY,
subject_id INT REFERENCES student_details(id),
tamil INT,
english int,
maths int
);

SELECT * FROM  subject ;

 CREATE OR REPLACE PROCEDURE add(a_id INTEGER,t_score INTEGER,e_score INTEGER,m_score INTEGER)
LANGUAGE plpgsql AS
$$
BEGIN
INSERT INTO subject (subject_id,tamil,english,maths) VALUES (a_id,t_score,e_score,m_score) ;
COMMIT ;
END ;
$$ ;

CALL add(5,25,30,20);

DROP TABLE subject ;

SELECT * FROM subject ;
































