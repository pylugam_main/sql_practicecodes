 CREATE OR REPLACE FUNCTION t_movie(movie_id VARCHAR)
RETURNS VARCHAR AS
$$
DECLARE
  movie_name VARCHAR ;
BEGIN
 SELECT type INTO movie_name FROM movies WHERE name = movie_id ;
 RETURN movie_name ;
END ;
$$
LANGUAGE plpgsql 

DROP FUNCTION  t_movie(movie_id integer)





SELECT * FROM t_movies ;
SELECT * FROM t_movie('e') ;